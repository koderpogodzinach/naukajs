JavaScript 
Informacje z kursu : 
 var pojemnik = 'Witaj w kursie JS';
//var pojemnik2 = 20.345;

// alert(20+20); 
  //  console.log(pojemnik); // wyświetlanie w konsoli 

var osoby = ['Jan Kowalski','Maria Nowak','Aldona Z']
console.log(osoby[0]);

osoby.push('Robert Lewandowski'); // dodawanie na końcu 
osoby.unshift('Milik Lewandowski'); // dodawanie na początku 
// oddawanie na dowolnym miejscu

osoby[9] = 'Krystyna'; // na miejscu 10 wstawiamy Krystyn 

console.log(osoby.slice(0,1)); // wypisanie pierwszej wartości

osoby.pop();  // usuwanie jednego elementu z końca tablicy 

osoby.shift();  // usuwnie z początku 

osoby.length; // zwraca długość tablicy 

console.log(osoby.join(' ; ')); // łączy całą tablice i oddzieli je wpisanym separatorem 

osoby.reverse();  // odwracanie tablicy 


//////// Tworzenie obiektów : 

var czlowiek = {
    wzrost: 178,
    kolorOczu: 'niebieski',
    
    umyjZeby: function() {
        console.log('Myje zeby');
    }
}

// Lekcja 3 DOM
// wybieranie elementów ze struktury DOM :
//document.getElementById('lista-art');

//document.getElementsByClassName('jumbotron');

//document.getElementsByTagName('p'); // na podstawie tagu p 


// nowsze metody 

//QuerySelector wybiera nam pierwszy selektor na jaki natrafi 
//console.log(document.querySelector('#lista-art article p'));

//QuerySelectorAll  - wybiera wszystkie 
//console.log(document.querySelectorAll('#lista-art article p'));


// z tymi elementami możemy zrobić praktycznie wszyszysko 
var listaArtykulow = document.getElementById('lista-art');

//możemy dodawać dowolne klasy 
listaArtykulow.classList.add('text-center', 'text-uppercase');
//usuwanie klasy 

listaArtykulow.classList.remove('text-center');

var title = document.querySelector('#lista-art h1');
title.textContent = 'Nowy tytuł';

var el = document.querySelector('#lista-art .jumbotron');
console.log(el.parentNode);  // wybieranie rodzica tego elementu
console.log(el.childNodes);  // wybieranie dzici tego elementu

var nowyElementP = document.createElement('p'); // tworzymy element p

nowyElementP.classList.add('text-center','text-uppercase');
nowyElementP.style.color = 'red';
nowyElementP.innerHTML = '<b>lorem ipsum</b> doler lipsum';

//do body dodajemy nowy dokument
document.body.appendChild(nowyElementP);

//kiedy znajdujemy się w rodzicu możemy usunąć dziecko 
nowyElementP.parentNode.removeChild(nowyElementP);

// Lekcja 4 
instrukcja If 

var wiek = 20;
if(wiek>18 || wiek = 23) { // || oznaczają lub  a symbol && oznacza "i"  
    alert('Jesteś dorosły lub prawei dorosły');
}else if {
    alert('Jestes malolatem');
}else {
    alert('Jeszcze trzeba to zweryfikaować')
}

// else if może być dowolnie dużo

//   === oznacza że sprawdzamy czy zmienna jest równa a takze czy jej typ się zgadza

var wiek = "18";

if(wiek === 18) {
    alert('Masz 18 lat');
}

var samochod = 'Mercedes';

switch (samochod) {
    case 'Mercedes':
        alert('Masz mercedesa');
        break;
    case 'Ferrari':
        alert('Masz Ferrari');
        break;
    default:
        alert('Masz nierozpoznawalny samochód');
                }
}

// lekcja 5 
skończyłem na 13,55 s 
https://www.youtube.com/watch?v=uUC8ky5ONJI&index=5&list=PLGjoxB-1BV8IKoG_fb934nZXSVi_v-4yg

//for(zmienne; warunek; zwiekszanie)

//for(var i = 1; i< 101; i++) {
//    console.log(i);
//}

var pracownicy = ['Marek','Edek','Stalowy','Omlet'];
//
//pracownicy.forEach(function(element,index,array) {
//    console.log(element);
//});

//petla while 

var index = 0;
var iluPracownikow = pracownicy.length;

while(iluPracownikow) {
  console.log(pracownicy[index] + ' Dostał zadanie')
    
    index++;
    iluPracownikow--;
}

// lekcja 6 funkcje 
function add(x,y) {
    return x+y;
}

console.log(add(2,3));